package de.ubt.ai4.petter.recpro.lib.attribute.modeling.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "attributeType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = RecproBinaryAttribute.class, name = "BINARY"),
        @JsonSubTypes.Type(value = RecproMetaAttribute.class, name = "META"),
        @JsonSubTypes.Type(value = RecproNumericAttribute.class, name = "NUMERIC"),
        @JsonSubTypes.Type(value = RecproTextualAttribute.class, name = "TEXT")
})
public class RecproAttribute {
    private String id;
    private String name;
    private String description;
    private RecproAttributeType attributeType;
    private String bpmsAttributeId;
    private boolean fromUrl;
    private String url;
}
