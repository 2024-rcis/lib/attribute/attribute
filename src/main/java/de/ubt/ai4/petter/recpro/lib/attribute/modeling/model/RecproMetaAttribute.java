package de.ubt.ai4.petter.recpro.lib.attribute.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class RecproMetaAttribute extends RecproAttribute {
    private RecproAttributeType attributeType = RecproAttributeType.META;
}
