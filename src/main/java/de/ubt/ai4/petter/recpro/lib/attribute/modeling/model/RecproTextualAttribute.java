package de.ubt.ai4.petter.recpro.lib.attribute.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RecproTextualAttribute extends RecproAttribute {
    private String defaultValue;
    private RecproAttributeType attributeType = RecproAttributeType.TEXT;
}
